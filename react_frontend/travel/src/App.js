import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';
import Home from './Components/Home'
import HotelList from "./Components/HotelList";
import Login from "./Components/Login";
import Signup from "./Components/SignUp";
import Forgot from "./Components/Forgot";
import Payment from "./Components/payment";
import Request from "./Components/request";

function App() {
  return (
    <BrowserRouter>
        <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/hotels' component={HotelList}/>
        <Route path='/signup' component ={Signup}/>
        <Route path='/login' component ={Login}/>
        <Route path ='/forgot' component={Forgot}/>
        <Route path='/request' component={Request}/>
        <Route path='/payment' compoent={Payment}/>
        </Switch>
    </BrowserRouter>
  );
}

export default App;
