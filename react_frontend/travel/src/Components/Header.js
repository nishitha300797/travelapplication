import { Component } from "react";

import { Link } from "react-router-dom";

import { BsSearch } from 'react-icons/bs';

import {CgProfile} from 'react-icons/cg'
// import MenuIcon from '@rsuite/icons/Menu';
// import {FiMenu} from 'react-icons/fi'

import { Dropdown } from 'rsuite'
import '../App.css'
class Header extends Component{
  
  render(){
    return(
      <header className="header">
    <div className="title">
      <img
        src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2522641/logo.png"
        className="logoo p-3" alt="logo"
      />
    </div>
    <div className="searchbar">
      <input type="text" />
      <button>
      <BsSearch/>
      </button>
    </div>
    <div className="nav">
    <Dropdown  icon={<CgProfile/>}  >
      <Link to='/signup'>
      <Dropdown.Item >Sign Up</Dropdown.Item>
      </Link>
      <Link to='/login'>
      <Dropdown.Item >Login</Dropdown.Item>
      </Link>
    <Dropdown.Item >Help</Dropdown.Item>
    </Dropdown>
      <Link to="/">Home</Link>
      {/* <div className="dropdown">
        <a
          className="btn  dropdown-toggle"
          href="#"
          role="button"
          id="dropdownMenuLink"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={16}
            height={16}
            fill="currentColor"
            className="bi bi-justify"
            viewBox="0 0 16 16"
          >
            <path
              fillRule="evenodd"
              d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
            />
          </svg>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={16}
            height={16}
            fill="currentColor"
            className="bi bi-person-circle"
            viewBox="0 0 16 16"
          >
            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
            <path
              fillRule="evenodd"
              d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
            />
          </svg>
        </a>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <li>
            <a className="dropdown-item" href="signup.html">
              Sign Up
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="login.html">
              Login
            </a>
          </li>
          <li>
            <a className="dropdown-item" href="#">
              Help
            </a>
          </li>
        </ul>
      </div> */}
    </div>
      </header>
    )
  }
}

export default Header