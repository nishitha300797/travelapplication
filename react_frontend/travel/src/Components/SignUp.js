import { Component } from "react";
// import '../signup.css'
import {Link} from 'react-router-dom'


class Signup extends Component{
  render(){
    return(
      <div className="container">
  <div className="toggle-button" onclick="window.location.href='login.html'">
    <p id="button-text">
      Sign Up
      <br />
      <Link  to='/login'>  Login</Link>
    
    </p>
  </div>
  <form>
    <div className="title">
      <h1 id="title-text">
        Welcome Back
        <br />
        Create an Account
      </h1>
    </div>
    <div className="double-field show-field" id="signUp-only">
      <span className="spacing" />
      <div className="input-div">
        <p>Username:</p>
        <input type="text" name="username" maxLength={30} />
      </div>
    </div>
    <p className="error">*Invalid Username</p>
    <div className="input-div">
      <p>Email:</p>
      <input type="email" name="email" />
      <p className="error">*Invalid email</p>
    </div>
    <div className="input-div">
      <p>Password:</p>
      <input type="password" name="password" />
      <p className="error">*password is too short (minimum of 8 characters)</p>
    </div>
    <div className="input-div show-field" id="signUp-only">
      <p>Confirm Password:</p>
      <input type="password" name="password_confirmation" />
      <p className="error">*password doesn't match</p>
    </div>
    <button id="button">Sign In</button>
  </form>
</div>

    )
  }
}

export default Signup