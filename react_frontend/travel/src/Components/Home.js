import { Component } from "react";
import { Places } from "../data";
import Place from "./Place";
import "../App.css";
import Header from './Header'
import Footer from "./Footer";

// import Carousel from "./Carousal";

class Home extends Component {
  render() {
    return (
      <>
        <Header/>
        <section className="hero">
          <div className="hero__background">
            <img
              src="/Images/home/home_background.png"
              alt="view from plane window"
            />
          </div>
          <div className="hero__content">
            <h2>Where will your next journey take you?</h2>
          </div>
        </section>
        <section className="content">
          <h2 className="section-title">Inspiration for your next trip</h2>
          <div className="card-grid" link to='/'>
            {Places.map((eachplace) => (
              <Place key={eachplace.id} place={eachplace} />
            ))}
          </div>
        </section>
        <Footer/>
        {/* <Carousel/> */}
      </>
    );
  }
}

export default Home;
