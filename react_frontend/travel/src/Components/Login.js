import { Component } from "react";
import {Link} from 'react-router-dom'
// import '../login.css'

class Login extends Component{
  render(){
    return(
      <div className="login-page">
  <div className="form">
    <h3>Login</h3>
    <form className="register-form">
      <input type="text" placeholder="name" />
      <p className="error">*User name already exists</p>
      <input type="password" placeholder="password" />
      <p className="error">*Password already exists</p>
      <input type="text" placeholder="email address" />
      <button>create</button>
      <p className="message">
        Already registered? 
        <Link to="/signup">
        <a href="#">Sign In</a>
        </Link>
       
      </p>
    </form>
    <form className="login-form">
      <input type="text" placeholder="username" />
      <p className="error">*User does not exist create a new account</p>
      <input type="password" placeholder="password" />
      <p className="error">*Password does not match</p>
      <button onclick="window.location.href='home.html'">login</button>
      <p className="message">
        Not registered?{" "}
        <a href="#" onclick="window.location.href='signup.html'">
          Create an account
        </a>
      </p>
      <Link to='/forgot'>
      <a href='#'>forgot password</a>
      </Link>
    
    </form>
  </div>
</div>

    )
  }
}

export default Login