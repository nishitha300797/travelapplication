import '../Req.css';
import {Component} from "react";


class Request extends Component{
  render(){
    return(
        <div>
        <div className="airbnb-logo">
          <img
            src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2522641/logo.png"
            className="logoo p-3"
          />
        </div>
        <hr />
        <div id="section1" className="book-section pt-5 pb-5">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-7 ">
                <h1 className="book-section">Request to book</h1>
                <hr />
                <div
                  className="contain"
                  style={{
                    width: 650,
                    height: 100,
                    border: "1px solid #ddd",
                    borderRadius: 10
                  }}
                >
                  <h6>This is a rare find</h6>
                  <p>Yogesh's place is usually book.</p>
                </div>
                <hr />
                <h2>Your trip</h2>
                <div className="d-flex flex-row">
                  <p className="mr-5">Dates</p>
                  <p>2-9 Apr</p>
                </div>
                <div className="d-flex flex-row">
                  <p className="mr-5">Guests</p>
                  <p>1 Guest</p>
                </div>
                <hr />
                <div className="login-in">
                  <h2>Log in or sign up to book</h2>
                  <div
                    style={{
                      width: 400,
                      height: 50,
                      border: "1px solid #ddd",
                      borderRadius: 10
                    }}
                    className="mb-3 d-flex flex-row"
                  >
                    <p className="pt-2">India(+91)</p>
                    <input
                      type="text"
                      style={{ width: 350 }}
                      placeholder="Phonenumber"
                    />
                  </div>
                  <p className="para2">
                    We’ll call or text you to confirm your number. Standard message
                    and data rates apply.
                  </p>
                  <button
                    className="btn btn-primary"
                    onclick="window.location.href='details.html'"
                  >
                    Back
                  </button>
                  <button
                    className="btn btn-success"
                    
                  >
                    Continue
                  </button>
                </div>
              </div>
              <div className="col-12 col-md-5">
                <div
                  style={{
                    width: 500,
                    height: 500,
                    border: "1px solid #ddd",
                    borderRadius: 20
                  }}
                >
                  <div className="d-flex flex-column">
                    <div className="pb-2 d-flex flex-row">
                      <div>
                        <img
                          src="https://a0.muscache.com/im/pictures/miso/Hosting-47052432/original/820199cd-b368-464a-a31e-9d1121257170.jpeg?im_w=720 "
                          className="image3 p-3 "
                        />
                      </div>
                      <div className="d-flex flex-column">
                        <div>
                          <p className="pt-3">private room in guest suite</p>
                          <h6>Mahalakshmi Residency</h6>
                        </div>
                        <div className="d-flex flex-row">
                          <span style={{ fontSize: "130%", color: "red" }}>★</span>
                          <p className="para4">4.95</p>
                          <span className="span1">(22 reviews)</span>
                        </div>
                      </div>
                    </div>
                    <div className="p-3">
                      <hr className="mr-4 ml-4" />
                      <h5>Price details</h5>
                      <p className="para3">Rs.7,000</p>
                      <p>Rs.1,000*7 nights</p>
                      <p className="para3 discount">-Rs.700</p>
                      <p>Weekly discount</p>
                      <p className="para3">Rs.889.42</p>
                      <p>Servicefee</p>
                      <p className="para3 total">Rs.7,189.42</p>
                      <p className="total1">Total(INR)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    
    )
}
}

export default Request;