import { Component } from "react";
import { Link } from "react-router-dom";
import '../App.css'

class Place extends Component {
  render() {
    const { place } = this.props;
    return (
      <>
      <Link  to ='/hotels'>
      <div  className="card">
          <div className="thumbnail">
            <img src={place.imageUrl} alt={place.name} />
          </div>
          <div className="card-content">
            <h3>{place.name}</h3>
          </div>
        </div>
      </Link>
        
      </>
    );
  }
}

export default Place;
