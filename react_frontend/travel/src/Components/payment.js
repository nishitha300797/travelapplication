import '../pay.css'
import {Component} from "react";


class Payment extends Component{
  render(){
    return(
      <>
      <h1>Welcome</h1>
      <div className="heading">
        <h1>Payment mode</h1>
      </div>
      <div className="container">
        <form action="">
          <div className="payment">
            <div className="payment-select">
              <div className="payment-select-header">
                <div className="payment-select-radio">
                  <input
                    type="radio"
                    href="section1"
                    id="debito"
                    name="pay"
                    defaultValue="debito"
                  />
                  <label htmlFor="debito">Debit</label>
                </div>
                <div className="payment-select-logo contain">
                  <img
                    src="http://pluspng.com/img-png/credit-card-png-credit-card-icons-e1426531278719-png-752.png"
                    alt=""
                    className="payment-select-image"
                  />
                </div>
              </div>
            </div>
            <div className="payment-select">
              <div className="payment-select-header">
                <div className="payment-select-radio">
                  <input
                    type="radio"
                    id="credito"
                    name="pay"
                    defaultValue="credito"
                  />
                  <label htmlFor="credito">Credit</label>
                </div>
                <div className="payment-select-logo contain">
                  <img
                    src="http://pluspng.com/img-png/credit-card-png-credit-card-icons-e1426531278719-png-752.png"
                    alt=""
                    className="payment-select-image"
                  />
                </div>
              </div>
            </div>
            <div className="payment-select">
              <div className="payment-select-header">
                <div className="payment-select-radio">
                  <input
                    type="radio"
                    id="paypal"
                    name="pay"
                    defaultValue="paypal"
                    defaultChecked=""
                  />
                  <label htmlFor="paypal">Paypal</label>
                </div>
                <div className="payment-select-logo contain">
                  <img
                    className="payment-select-image"
                    src="https://i2.wp.com/2017.singapore.wordcamp.org/files/2017/10/PayPal.png?resize=1200%2C630&ssl=1"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="payment-select">
              <div className="payment-select-header">
                <div className="payment-select-radio">
                  <input
                    type="radio"
                    id="paypal"
                    name="pay"
                    defaultValue="paypal"
                    defaultChecked=""
                  />
                  <label htmlFor="paypal">Via cash</label>
                </div>
                <div className="payment-select-logo contain">
                  <img
                    className="payment-select-image image"
                    src="https://thumbs.dreamstime.com/b/vector-dollar-sign-money-icon-currency-bill-symbol-172063650.jpg"
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div className="paywithcard">
        <div className="d-flex justify-content-between align-items-center">
          <h3 className="fs-5 fs-5 m-0"> Pay with credit/debit card </h3>
          <span>
            {" "}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width={50}
              viewBox="0 0 512 512"
              style={{ enableBackground: "new 0 0 512 512" }}
              xmlSpace="preserve"
            >
              <path
                style={{ fill: "#2196f3" }}
                d="M53.333 85.333h405.333c29.455 0 53.333 23.878 53.333 53.333v234.667c0 29.455-23.878 53.333-53.333 53.333H53.333C23.878 426.667 0 402.789 0 373.333V138.667c0-29.456 23.878-53.334 53.333-53.334z"
              />
              <path style={{ fill: "#455a64" }} d="M0 149.333h512v85.333H0z" />
              <path
                style={{ fill: "#fafafa" }}
                d="M160 320H74.667C68.776 320 64 315.224 64 309.333s4.776-10.667 10.667-10.667H160c5.891 0 10.667 4.776 10.667 10.667S165.891 320 160 320zM224 362.667H74.667C68.776 362.667 64 357.891 64 352s4.776-10.667 10.667-10.667H224c5.891 0 10.667 4.776 10.667 10.667s-4.776 10.667-10.667 10.667z"
              />
            </svg>{" "}
          </span>
        </div>
        <div className="row g-3 alignments">
          <div className="col-md-12 mb-3 align">
            <label className="form-label" htmlFor="cardnumber">
              {" "}
              Card Number{" "}
            </label>
            <input
              type="text"
              className="form-control"
              name="cardnumber"
              id="cardnumber"
              maxLength={16}
            />
          </div>
          <div className="col-6 align">
            <label className="form-label" htmlFor="expirationdate">
              {" "}
              Expiration <span className="text-muted small">(mm/yy)</span>
            </label>
            <input
              type="text"
              className="form-control"
              id="expirationdate"
              name="expirationdate"
              inputMode="numeric"
              pattern="[0-9]*"
              maxLength={5}
              placeholder="10/23"
            />
          </div>
          <div className="col-6 align">
            <label className="form-label" htmlFor="securitycode">
              <span>Security Code </span>{" "}
              <span className="text-muted small">CVC/ CVV</span>
            </label>
            <input
              type="text"
              className="form-control"
              name="securitycode"
              inputMode="numeric"
              pattern="[0-9]*"
              maxLength={4}
              placeholder={1234}
            />
          </div>
        </div>
        <div className="button">
          <button onclick="window.location.href='request.html'" className="btn2">
            back
          </button>
          <button className="btn1">Next</button>
        </div>
      </div>
    </>
    
    )
}
}

export default Payment;
