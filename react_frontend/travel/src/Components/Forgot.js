import {Component} from 'react'
import {Link} from 'react-router-dom'


class Forgot extends Component{
  render(){
    return(
      <div className="row">
  <h1>Forgot Password</h1>
  <h6 className="information-text">
    Enter your registered email to reset your password.
  </h6>
  <div className="form-group">
    <input type="email" name="user_email" id="user_email" />
    <p className="para">* Invalid Email Address</p>
    <p>
      <label htmlFor="username">Email</label>
    </p>
    <button onclick="window.location.href='login.html'">Reset Password</button>
  </div>
  <div className="footer">
    <h5>
      New here? 
      <Link to='/signup'>
      <a href="#">Sign Up.</a>
      </Link>
     
    </h5>
    <h5>
    Already have an account?
      <Link to='/login'>
      <a href="#">Sign In.</a>
      </Link>
      
    </h5>
  </div>
</div>

    )
  }
}

export default Forgot