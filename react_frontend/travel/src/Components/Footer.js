function Footer() {
  return (
    <footer className="footer">
      <div className="footer-grid">
        <div className="column">
          <h5>Support</h5>
          <p>Help Center</p>
          <p>Safety Information</p>
          <p>Cancellation Option</p>
          <p>Our Covid19 Response</p>
        </div>
        <div className="column">
          <h5>Community</h5>
          <p>Airbnb.org: disaster relief housing</p>
          <p>Support Afghan refugees</p>
          <p>Combating discrimination</p>
        </div>
        <div className="column">
          <p>
            <a href="#">About</a>
          </p>
          <p>
            <a href="#">Explore</a>
          </p>
          <p>
            <a href="#">Privacy Policy</a>
          </p>
          <p>
            <a href="#">Contact</a>
            <a href="#" />
          </p>
        </div>
      </div>
      <div>
        <div>
          <a href="#">© 2022 Airbnb, Inc.</a>
          <a href="#"> · Privacy</a>
          <a href="#"> · Terms</a>
          <a href="#"> · Sitemap</a>
          <a href="#"> · Company details</a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
