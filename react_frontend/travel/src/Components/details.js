import {Component} from "react";
import "../App.css";
class Details extends Component{
  render() {
    const {data} = this.props
    return(
      <div>
        {/* This is how HTML comments look like */}
        {/* the title will appear on the page*/}
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossOrigin="anonymous" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossOrigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/details.css" />
        <header className="header">
          <div className="title">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2522641/logo.png" className="logoo p-3" />
          </div>
          <div className="searchbar">
            <input type="text" />
            <button><i className="fas fa-search" /></button>
          </div>
          <div className="nav">
            <a href="#">Home</a>
            <div className="dropdown">
              <a className="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-justify" viewBox="0 0 16 16">
                  <path fillRule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-person-circle" viewBox="0 0 16 16">
                  <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                  <path fillRule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                </svg>
              </a>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><a className="dropdown-item" href="#">Sign Up</a></li>
                <li><a className="dropdown-item" href="#">Login</a></li>
                <li><a className="dropdown-item" href="#">Help</a></li>
              </ul>
            </div>
          </div>
        </header>
        <div className="container d-flex flex-column justify-content-start">
          <div className="row">
            <div className="col-12">
              <h1 className="heading">
                Modern &amp; well-equipped Apt. 7 mins to the beach
              </h1>
              <div className="d-flex flex-row justify-content-start">
                <svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} fill="red" className="bi bi-star" viewBox="0 0 16 16">
                  <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                </svg>
                <p className="ml-3">4.75</p>
                <p>53 reviews</p>
                <p>Candolim,Goa,India</p>
              </div>
              <div>
                <img src={data.imageUrl1} className="image2" />
                <img src={data.imageUrl2} className="image ml-3" />
                <img src={data.imageUrl} className="image ml-3 mt-3" />
                <img src={data.imageUrl4} className="image mt-3 ml-3" />
              </div>
            </div>
          </div>
          {/* <button onclick="window.location.href='request.html'" class="btn1">
        Reserve
      </button> */}
        </div>
        <div className="container">
          <div className="row">
            <div className="col-8">
              <div className="d-flex flex-row">
                <div>
                  <h1 className="heading1 mr-5 mt-4">{data. hosted_by}</h1>
                  <p>{data.details}</p>
                </div>
                <img src="https://a0.muscache.com/im/pictures/user/0e511d62-9e18-4fe9-ac4f-41481f37a27a.jpg?im_w=240" className="profile ml-5 mt-4" />
              </div>
              <hr />
              <div className="d-flex flex-column">
                <div className="d-flex flex-row">
                  <svg xmlns="http://www.w3.org/2000/svg" width={19} height={19} fill="currentColor" className="bi bi-stars" viewBox="0 0 16 16 p-2">
                    <path d="M7.657 6.247c.11-.33.576-.33.686 0l.645 1.937a2.89 2.89 0 0 0 1.829 1.828l1.936.645c.33.11.33.576 0 .686l-1.937.645a2.89 2.89 0 0 0-1.828 1.829l-.645 1.936a.361.361 0 0 1-.686 0l-.645-1.937a2.89 2.89 0 0 0-1.828-1.828l-1.937-.645a.361.361 0 0 1 0-.686l1.937-.645a2.89 2.89 0 0 0 1.828-1.828l.645-1.937zM3.794 1.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387A1.734 1.734 0 0 0 4.593 5.69l-.387 1.162a.217.217 0 0 1-.412 0L3.407 5.69A1.734 1.734 0 0 0 2.31 4.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387A1.734 1.734 0 0 0 3.407 2.31l.387-1.162zM10.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.156 1.156 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.156 1.156 0 0 0-.732-.732L9.1 2.137a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732L10.863.1z" />
                  </svg>
                  <div>
                    <h1 className="heading1 ml-3">{data.name1}</h1>
                    <p className="para ml-3">
                     {data.description}
                    </p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-door-open" viewBox="0 0 16 16">
                    <path d="M8.5 10c-.276 0-.5-.448-.5-1s.224-1 .5-1 .5.448.5 1-.224 1-.5 1z" />
                    <path d="M10.828.122A.5.5 0 0 1 11 .5V1h.5A1.5 1.5 0 0 1 13 2.5V15h1.5a.5.5 0 0 1 0 1h-13a.5.5 0 0 1 0-1H3V1.5a.5.5 0 0 1 .43-.495l7-1a.5.5 0 0 1 .398.117zM11.5 2H11v13h1V2.5a.5.5 0 0 0-.5-.5zM4 1.934V15h6V1.077l-6 .857z" />
                  </svg>
                  <div>
                    <h1 className="heading1 ml-3">{data.name2}</h1>
                    <p className="para ml-3">{data.description}</p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-calendar-event" viewBox="0 0 16 16">
                    <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z" />
                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                  </svg>
                  <div>
                    <h1 className="heading1 ml-3">{data.name3}</h1>
                  </div>
                </div>
              </div>
              <div>
                <p>
                budget low key rooms ideal for couples and solo travellers  has air conditioning, wifi access , tv with tata sky connection  24x7 hot water supply . housekeeping on request during the day  free onsite parking is available
                </p>
                <a href style={{color: 'black'}}>Show More</a>
              </div>
              <hr />
              <div className="d-flex flex-column">
                <h1 className="heading2">Beds available</h1>
                <div>
                  <img src="https://a0.muscache.com/im/pictures/f5731f7e-518a-4d7a-ac67-16f1733edddc.jpg?im_w=320" />
                  <h1 className="head pt-3">Bedroom</h1>
                  <p>1 queen bed</p>
                </div>
              </div>
              <hr />
              <div>
                <h4>What this place offers</h4>
                <div className="d-flex flex-row">
                  <div className="col-4">
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-egg" viewBox="0 0 16 16">
                        <path d="M8 15a5 5 0 0 1-5-5c0-1.956.69-4.286 1.742-6.12.524-.913 1.112-1.658 1.704-2.164C7.044 1.206 7.572 1 8 1c.428 0 .956.206 1.554.716.592.506 1.18 1.251 1.704 2.164C12.31 5.714 13 8.044 13 10a5 5 0 0 1-5 5zm0 1a6 6 0 0 0 6-6c0-4.314-3-10-6-10S2 5.686 2 10a6 6 0 0 0 6 6z" />
                      </svg>
                      <p className="heading1 ml-3">Kitchen</p>
                    </div>
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-bicycle" viewBox="0 0 16 16">
                        <path d="M4 4.5a.5.5 0 0 1 .5-.5H6a.5.5 0 0 1 0 1v.5h4.14l.386-1.158A.5.5 0 0 1 11 4h1a.5.5 0 0 1 0 1h-.64l-.311.935.807 1.29a3 3 0 1 1-.848.53l-.508-.812-2.076 3.322A.5.5 0 0 1 8 10.5H5.959a3 3 0 1 1-1.815-3.274L5 5.856V5h-.5a.5.5 0 0 1-.5-.5zm1.5 2.443-.508.814c.5.444.85 1.054.967 1.743h1.139L5.5 6.943zM8 9.057 9.598 6.5H6.402L8 9.057zM4.937 9.5a1.997 1.997 0 0 0-.487-.877l-.548.877h1.035zM3.603 8.092A2 2 0 1 0 4.937 10.5H3a.5.5 0 0 1-.424-.765l1.027-1.643zm7.947.53a2 2 0 1 0 .848-.53l1.026 1.643a.5.5 0 1 1-.848.53L11.55 8.623z" />
                      </svg>
                      <p className="heading1 ml-3">Free on-street parking</p>
                    </div>
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                        <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z" />
                      </svg>
                      <p className="heading1 ml-3">Lift</p>
                    </div>
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-door-open" viewBox="0 0 16 16">
                        <path d="M8.5 10c-.276 0-.5-.448-.5-1s.224-1 .5-1 .5.448.5 1-.224 1-.5 1z" />
                        <path d="M10.828.122A.5.5 0 0 1 11 .5V1h.5A1.5 1.5 0 0 1 13 2.5V15h1.5a.5.5 0 0 1 0 1h-13a.5.5 0 0 1 0-1H3V1.5a.5.5 0 0 1 .43-.495l7-1a.5.5 0 0 1 .398.117zM11.5 2H11v13h1V2.5a.5.5 0 0 0-.5-.5zM4 1.934V15h6V1.077l-6 .857z" />
                      </svg>
                      <p className="heading1 ml-3">Patio or balcony</p>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-wifi" viewBox="0 0 16 16">
                        <path d="M15.384 6.115a.485.485 0 0 0-.047-.736A12.444 12.444 0 0 0 8 3C5.259 3 2.723 3.882.663 5.379a.485.485 0 0 0-.048.736.518.518 0 0 0 .668.05A11.448 11.448 0 0 1 8 4c2.507 0 4.827.802 6.716 2.164.205.148.49.13.668-.049z" />
                        <path d="M13.229 8.271a.482.482 0 0 0-.063-.745A9.455 9.455 0 0 0 8 6c-1.905 0-3.68.56-5.166 1.526a.48.48 0 0 0-.063.745.525.525 0 0 0 .652.065A8.46 8.46 0 0 1 8 7a8.46 8.46 0 0 1 4.576 1.336c.206.132.48.108.653-.065zm-2.183 2.183c.226-.226.185-.605-.1-.75A6.473 6.473 0 0 0 8 9c-1.06 0-2.062.254-2.946.704-.285.145-.326.524-.1.75l.015.015c.16.16.407.19.611.09A5.478 5.478 0 0 1 8 10c.868 0 1.69.201 2.42.56.203.1.45.07.61-.091l.016-.015zM9.06 12.44c.196-.196.198-.52-.04-.66A1.99 1.99 0 0 0 8 11.5a1.99 1.99 0 0 0-1.02.28c-.238.14-.236.464-.04.66l.706.706a.5.5 0 0 0 .707 0l.707-.707z" />
                      </svg>
                      <p className="heading1 ml-3">Wifi</p>
                    </div>
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-tv" viewBox="0 0 16 16">
                        <path d="M2.5 13.5A.5.5 0 0 1 3 13h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zM13.991 3l.024.001a1.46 1.46 0 0 1 .538.143.757.757 0 0 1 .302.254c.067.1.145.277.145.602v5.991l-.001.024a1.464 1.464 0 0 1-.143.538.758.758 0 0 1-.254.302c-.1.067-.277.145-.602.145H2.009l-.024-.001a1.464 1.464 0 0 1-.538-.143.758.758 0 0 1-.302-.254C1.078 10.502 1 10.325 1 10V4.009l.001-.024a1.46 1.46 0 0 1 .143-.538.758.758 0 0 1 .254-.302C1.498 3.078 1.675 3 2 3h11.991zM14 2H2C0 2 0 4 0 4v6c0 2 2 2 2 2h12c2 0 2-2 2-2V4c0-2-2-2-2-2z" />
                      </svg>
                      <p className="heading1 ml-3">TV</p>
                    </div>
                    <div className="d-flex flex-row">
                      <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-fan" viewBox="0 0 16 16">
                        <path d="M10 3c0 1.313-.304 2.508-.8 3.4a1.991 1.991 0 0 0-1.484-.38c-.28-.982-.91-2.04-1.838-2.969a8.368 8.368 0 0 0-.491-.454A5.976 5.976 0 0 1 8 2c.691 0 1.355.117 1.973.332.018.219.027.442.027.668Zm0 5c0 .073-.004.146-.012.217 1.018-.019 2.2-.353 3.331-1.006a8.39 8.39 0 0 0 .57-.361 6.004 6.004 0 0 0-2.53-3.823 9.02 9.02 0 0 1-.145.64c-.34 1.269-.944 2.346-1.656 3.079.277.343.442.78.442 1.254Zm-.137.728a2.007 2.007 0 0 1-1.07 1.109c.525.87 1.405 1.725 2.535 2.377.2.116.402.222.605.317a5.986 5.986 0 0 0 2.053-4.111c-.208.073-.421.14-.641.199-1.264.339-2.493.356-3.482.11ZM8 10c-.45 0-.866-.149-1.2-.4-.494.89-.796 2.082-.796 3.391 0 .23.01.457.027.678A5.99 5.99 0 0 0 8 14c.94 0 1.83-.216 2.623-.602a8.359 8.359 0 0 1-.497-.458c-.925-.926-1.555-1.981-1.836-2.96-.094.013-.191.02-.29.02ZM6 8c0-.08.005-.16.014-.239-1.02.017-2.205.351-3.34 1.007a8.366 8.366 0 0 0-.568.359 6.003 6.003 0 0 0 2.525 3.839 8.37 8.37 0 0 1 .148-.653c.34-1.267.94-2.342 1.65-3.075A1.988 1.988 0 0 1 6 8Zm-3.347-.632c1.267-.34 2.498-.355 3.488-.107.196-.494.583-.89 1.07-1.1-.524-.874-1.406-1.733-2.541-2.388a8.363 8.363 0 0 0-.594-.312 5.987 5.987 0 0 0-2.06 4.106c.206-.074.418-.14.637-.199ZM8 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z" />
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14Zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16Z" />
                      </svg>
                      <p className="heading1 ml-3">Air Conditioning</p>
                    </div>
                    <div className="d-flex flex-row">
                      <i className="fa-solid fa-car" />
                      <p className="heading1 ml-3">Hair dryer</p>
                    </div>
                  </div>
                </div>
                <a href="#" style={{color: 'black'}}>Show all 23 amenities</a>
              </div>
            </div>
            <div className="col-4">
              <div className="card">
                <div className="d-flex flex-row p-3">
                  <h3>1,151 <span className="para">/night</span></h3>
                  <svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} fill="red" className="bi bi-star ml-4 mt-2" viewBox="0 0 16 16">
                    <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                  </svg>
                  <p className="ml-2 mt-2">{data.reviews}</p>
                </div>
                <div className="d-flex justify-content-center">
                  <div className="card1 p-2">
                    <h1 className="card-heading">Check-In</h1>
                    <p className="para">4/21/2022</p>
                  </div>
                  <div className="card1 p-2">
                    <h1 className="card-heading">Check-Out</h1>
                    <p className="para">4/28/2022</p>
                  </div>
                </div>
                <div className="container p-2 d-flex justify-content-center">
                  <select className=" drop p-2 mt-3">
                    <option value="Guests"> Guests
                    </option>
                    <option value="Mercedes"> Adults
                    </option>
                    <option value="Audi"> Children
                    </option>
                    <option value="Skoda"> pets
                    </option>
                  </select>
                </div>
                <div className="d-flex justify-content-center">
                  <button className="button" onclick="window.location.href='request.html'">Reserve</button>
                </div>
                <p className="para1 p-2">You won't be charged yet</p>
                <div className="container">
                  <div className="d-flex flex-row">
                    <div className="d=flex flx-column mr-5">
                      <p>₹1,151 x 7 nights</p>
                      <p>ServicesFee</p>
                    </div>
                    <div className="d=flex flx-column ml-5">
                      <p>₹8,060</p>
                      <p>₹1,138</p>
                    </div>
                  </div>
                  <hr />
                  <p>Total before taxes</p>
                  <p>₹9,198</p>  
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <hr />
            <div className="row">
              <div className="col-12">
                <div className="d-flex flex-row">
                  <svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} fill="red" className="bi bi-star" viewBox="0 0 16 16">
                    <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                  </svg>
                  <h5 className="heading2 ml-1">4.75 53 reviews</h5>
                </div>
              </div>
              <div className="col-8">
                <p>Cleanliness <span>4.8</span></p>
                <p>Communication <span>4.8</span></p>
                <p>Check-in <span>5</span></p>
              </div>
              <div className="col-4">
                <p>Accuracy <span>4.6</span></p>
                <p>Location <span>4.5</span></p>
                <p>Value <span>5</span></p>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image1} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name1}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>
                     {data.review1}
                    </p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image2} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name2}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>
                    {data.review2}
                    </p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image3} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name3}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>
                    {data.review3}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-6">
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image4} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name4}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>{data.review4}</p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image5} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name5}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>
                    {data.review5}
                    </p>
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <div className="d-flex flex-column">
                    <div className="d-flex flex-row">
                      <img src={data.image6} className="profile" />
                      <div>
                        <h1 className="heading1 ml-3">{data.name6}</h1>
                        <p className="ml-3">{data.date}</p>
                      </div>
                    </div>
                    <p>
                    {data.review6}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <a href="#" className="link mt-4">Show all Reviews</a>
          </div>
          <hr />
          <div className="container">
            <h3>Where you’ll be</h3>
            <div className="map mt-4">
              <iframe width="80%" height={400} src="https://maps.google.com/maps?width=100%&height=600&hl=en&coord=52.70967533219885, -8.020019531250002&q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland&ie=UTF8&t=&z=14&iwloc=B&output=embed" frameBorder={0} scrolling="no" marginHeight={0} marginWidth={0} /><br />
            </div>
            <div className="container">
              <div className="row">
                <h1 className="heading1 mt-4">Candolim, Goa, India</h1>
                <p>
                  The neighborhood is very safe. the apartment is in a building in a
                  shared compound with our family home and is located on the 2nd
                  floor. The other flats are serviced apartments too. The building has
                  an Elevator and you will have complete privacy.
                </p>
              </div>
            </div>
          </div>
        </div>
        <Footer/>
      </div>


    )
    
  }
}

export default Details 




