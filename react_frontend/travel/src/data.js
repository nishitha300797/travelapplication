const Places = [
  {
    id: 1,
    name: "Puducherry",
    imageUrl:
      "https://a0.muscache.com/im/pictures/06248faf-fafa-4adb-9447-3a393ca625a4.jpg?im_w=320",
  },
  {
    id: 2,
    name: "Ooty",
    imageUrl:
      "https://a0.muscache.com/im/pictures/aef20929-0d6a-40e7-8ac9-321ff0edf8c9.jpg?im_q=highq&im_w=720",
  },
  {
    id: 3,
    name: "Benguluru",
    imageUrl:
      "https://a0.muscache.com/im/pictures/db8167f7-5c57-4684-80ae-4350c73e45ef.jpg?im_q=highq&im_w=720",
  },
  {
    id: 4,
    name: "Kodaikanal",
    imageUrl:
      "https://a0.muscache.com/im/pictures/73250991-433e-4950-b7d1-59bba711bb57.jpg?im_q=highq&im_w=720",
  },
];

const Hotel_images = [
  {
    id: 1,
    imageUrl1:
      "https://a0.muscache.com/im/pictures/c0c058e6-40c1-4295-b260-467d63e05351.jpg?im_w=720",
  },
  {
    id: 2,
    imageUrl2:
      "https://a0.muscache.com/im/pictures/865cb0b8-5d7c-4a0d-900f-78e799e55e93.jpg?im_w=720",
  },
  {
    id: 3,
    imageUrl3:
      "https://a0.muscache.com/im/pictures/931352c1-fb67-46ee-9d2f-1cef2957053f.jpg?im_w=720",
  },
  {
    id: 4,
    imageUrl4:
      "https://a0.muscache.com/im/pictures/c034dce7-f83b-4ca4-96bf-2f11da62a642.jpg?im_w=720",
  },
];
const Hotel_details = [
  {
    id: 1,
    name1: "Enhanced Clean",
    description:
      "his host has committed to Airbnb's 5-step enhanced cleaning process",
  },
  {
    id: 2,
    name2: "Self check-in",
    description: "Check yourself in with the lockbox",
  },
  {
    id: 3,
    name3: "Free cancellation for 48 hours",
  },
];
const Reviews=[
  {
    id:1,
    image1:"https://a0.muscache.com/im/pictures/user/8023ee79-ac4e-4d9b-bcaa-2c49ab0e1f42.jpg?im_w=240",
    name1:"Vinayak",
    date:"February 2022",
    review1:" Place is nice .. little more work on  cleanliness will make it much better.."
  },
   {
     id:2,
     image2:"https://a0.muscache.com/im/pictures/user/e16defd9-203e-495e-b3df-c5e326a7f688.jpg?im_w=240",
     name2:"Manish",
     date:"October 2022",
     review2:"This place is in the heart of Candolim with everything easily accessible. Great place for staycation"
    
   },
   {
     id:3,
     image3:"https://a0.muscache.com/im/pictures/user/6a8f3e1b-d9d4-4709-a1b7-5b5a8d2a829a.jpg?im_w=240" ,
     name3:"Dinesh",
     date:"january 2022",
     review3:"let's talk about the listing first! it's located in the heart of Candolim from where you will have no problem accessing the  famous tourist spots by taxi, rental scooty Or by walking as"
   },
   {
     id:4,
     image4:"https://a0.muscache.com/im/pictures/user/b50226a5-acba-49cc-a5ae-4a26760477a4.jpg?im_w=240",
     name4:"Sandeep",
     date:"March 2020",
     review4:"Good for budget stay"
   },
   {
     id:5,
     image5:"https://a0.muscache.com/im/pictures/user/328c4b1a-20b1-4645-84b8-af1a60fcafc6.jpg?im_w=240",
     name5:"Suresh",
     date:"januray 2022",
     review5:" the place is as described, perfect for a couple.. all the eating  places n beach are walking distance from here..area is  peaceful.. highly recommended"
   },
   {
     id:6,
     image6:"https://a0.muscache.com/im/pictures/user/4d2c7f11-8472-4714-b288-52df95f0b3f4.jpg?im_w=240",
     name6:"Shanumuk",
     date:"August 2019",
     review6:" Sanaa's place is a hidden gem nestled in a residential neighborhood away from the hustle and bustle of the crowded  streets, yet a stone throw distance from all the cool"
   }
]
  
const Host={
    hosted_by:"Entire rental unit hosted by Sanaa",
    details:"2 guests1 bedroom1 bed1 bathroom"
}

const Hotels=[
  {
    id:1,
    heading1:"Entire rental Unit in Calungute",
     para1:"BeachWalk Bliss by Laze Around Us",
     rooms1:"3guests . 1bedroom . 1bed . 1bathroom . Wifi . Air Conditioning",
     amount1:"Rs. 1,523/night",
     reviews1:" 4.69 (52 Reviews)"
  },
  {
    id:2,
    heading2:"Entire apartment in Calungute",
    para2:"Stylish Studio Apartment with highspeed",
    rooms2:"3guests . 1bedroom . 1bed . 1bathroom . Wifi . pool . kitchen",
    amount2:"Rs. 867/night",
    reviews2:"4.81 (32 Reviews)"
  },
  {
    id:3,
    heading3:"Entire service apartment in Arpora",
    para3:"Cozy WFH 1BHK with pool nr Calangute Beach",
    rooms3:"4 guests . 1bedroom . 1bed . 5bathrooms . pool . kitchen . Wifi",
    amount3:"Rs. 1,455/night",
    reviews3:" 5.0 (4 Reviews)"
  }
  
]

const Hotelslist=[

  {

    id:1,

    image:"https://a0.muscache.com/im/pictures/miso/Hosting-35260255/original/088698fa-eacb-47b1-ac94-9b4fae3fc929.jpeg?im_w=960",

    name:"Entire rental Unit in Calungute",

     para1:"BeachWalk Bliss by Laze Around Us",

     para2:"3guests . 1bedroom . 1bed . 1bathroom . Wifi . Air Conditioning",

     amount:"Rs. 1,523/night",

     reviews:"4.69 (52 Reviews)"

  },

  {

    id:2,

    name:"Entire apartment in Calungute",

    image:"https://a0.muscache.com/im/pictures/miso/Hosting-35260255/original/c16d6170-e995-4449-bb07-d8d677a935d8.jpeg?im_w=1200",

    para1:"Stylish Studio Apartment with highspeed",

    para2:"3guests . 1bedroom . 1bed . 1bathroom . Wifi . pool . kitchen",

    amount:"Rs. 867/night",

    reviews:" 4.81 (32 Reviews)"

  },

  {

    id:3,

    image:"https://a0.muscache.com/im/pictures/miso/Hosting-35260255/original/e15f858a-aa7e-4f5c-9224-a3d7ce077177.jpeg?im_w=1200",

    name:"Entire service apartment in Arpora",

    para1:"Cozy WFH 1BHK with pool nr Calangute Beach",

    para2:"4 guests . 1bedroom . 1bed . 5bathrooms . pool . kitchen . Wifi",

    amount:"Rs. 1,455/night",

    reviews:"5.0 (4 Reviews)"

  }

 

]
 



export { Places, Hotel_images, Hotelslist, Hotel_details,Reviews,Host, Hotels};
